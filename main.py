import argparse
import sys
import yaml
import requests

from RESTester.RESTestGenerator import generateSimpleTest, loadTestCase, getStructure
from RESTester.test import testSimpleWithGeneratedData

parser = argparse.ArgumentParser()
parser.add_argument('--yaml', help='swagger directory', type=str, default='')
parser.add_argument('--json', help='json directory', type=str, default='')
parser.add_argument('--url', help='swagger url', type=str, default='')
parser.add_argument('--testfile', help='test saved testcases', type=str, default='')

args = parser.parse_args(sys.argv[1:])


def init() -> dict:
    api = None
    if args.yaml != '':
        api = open(args.yaml, 'r').read()
    elif args.json != '':
        api = open(args.json, 'r').read()
    elif args.url != '':
        api = requests.request(url=args.url, method='GET').content
    else:
        print('ERROR : You should Input least one file. Swagger YAML or JSON')
        print('python main.py (--yaml <YAML> | --json <JSON> | --url <URL>) [ --testfile <TESTFILE> ]')
        exit(-1)

    swagger = yaml.safe_load(api)

    return swagger


if __name__ == "__main__":
    swagger = init()

    if args.testfile == '':
        test = generateSimpleTest(swagger)
    else:
        test = loadTestCase(args.testfile)
    struct = getStructure(swagger)
    testSimpleWithGeneratedData(swagger, test, struct)
