# RESTester #
OpenAPI 2.0을 기반으로 작성된 Swagger 파일을 읽고, 송수신하는 인자를 분석하여
적절한 예시 인자를 생성하여 테스트하는 프로그램

USAGE

    python main.py (--yaml <YAML> | --json <JSON> | --url <URL>) [ --testfile <TESTFILE> ]


## reader ##

---
Swagger 기본 정보를 읽어오는 모듈
- base path
- request path
- parameters
- responses
- produces

위 항목들을 읽어옴


## component (generator) ##

---
Swagger 요청에 필요한 인자를 읽어서 객체 형태로 만들어주는 모듈

### Core class ###

---
#### Component  #### 
하나의 값을 나타내는 추상클래스
##### get() #####
컴포넌트의 값을 str, int, dict, list 등의 형태로 변환
##### serialize() #####
현재 컴포넌트 및 하위 컴포넌트들을 직렬화해서 리스트로 반환
##### default_value #####
컴포넌트의 기본값
##### name #####
중첩된 컴포넌트의 전체 이름 ex) body:Pet.id

### Diagram ###

---
![Component](./Component.png)


### Classes ###

---


- Component : 하나의 값을 나타내는 추상클래스
- Countable : MIN, MAX를 가지는 추상클래스
- RequireRandom : 요구사항을 가지고 있는 추상클래스
- Random : 완전 난수를 가지는 추상클래스


- Components : 컴포넌트 집합
- Body : body에 해당되는 컴포넌트 집합
- Query : query에 해당되는 컴포넌트 집합


- Object : dict
- Array : list 
- String : str
- Integer : int
- Number : float
- File : binary file
- Boolean : bool

### Functions ###

---
#### generateComponent(name: str, schema: dict) -> Component ####
컴포넌트에 대한 dict가 들어오면, 그 구조를 읽어서 컴포넌트 생성

#### findReference(name: str, swagger_ref: dict) -> Component ####
$ref를 읽어서 대응되는 컴포넌트를 생성

#### generateReqeustArgument(parameters: list) -> Dict[str, Components] ####
body, query, path 등의 요청 정보 리스트를 받아서, 각 요청 정보에 대해 컴포넌트 집합을 생성

#### parseParameter(name: str, parameter: dict) -> Component ####
요청에서 요구하는 인자에 대한 dict를 받아서 컴포넌트를 생성

#### queriesToQueryString(json_queries: dict) -> str ####
dict 형태로 된 GET queries를 string으로 변환해줌

## RESTestGenerator ##

---
### generateSimpleTest(swagger: dict) -> dict ###
파싱해서 dict 형태로 존재하는 swagger를 읽어서 테스트 케이스 생성 절차 수행


## test ##

---
### testSimpleWithGeneratedData(swagger: dict, test: dict, args: dict) ###
생성된 테스트 케이스를 기반으로 테스트 절차 수행(요청, 검증)


## request ##

---
### request(method: str, path: str, data: str = None, params: dict = None, files: dict = None, header: dict = None, content_list: List[str] = None) -> Response ###
컴포넌트의 값을 가져와서 실제 요청을 시행


## validator ##

---
### validateResultWithTestCase(response: Response, expected_response: int) -> str ###
결과 검증


## output ##

---
파일, 콘솔 출력 지원 모듈

### reportRequestAndResponseWithTestCase(basePath: str, context: dict, test_count: Dict[str, int]) ###
테스트 케이스를 통해서 수행된 테스트의 결과 페이지 생성

### saveTest(test: dict) ###
생성된 테스트 케이스를 저장
