"""
Swagger to JSON
Dependent on OpenAPI 2
Not compatible with OpenAPI 3.
"""
from abc import ABC, abstractmethod
from datetime import datetime
from enum import Enum
from random import random, randrange
import yaml
import pathlib
import random
from typing import Dict, List
from xeger import Xeger


def openFileFromCurrentDir(filename: str, mod: str = 'r'):
    return open('{}/{}'.format(str(pathlib.Path(__file__).parent), filename), mod)


class Generator:
    with openFileFromCurrentDir('SWAGGER.yml') as yml:
        SWAGGER = yaml.safe_load(yml)

    def __init__(self):
        self.swagger = None
        self.file_list = dict()


generator = Generator()


def loadSwaggerJSON(swagger: dict) -> None:
    global generator
    generator.swagger = swagger
    generator.file_list = dict()


class ValueType(Enum):
    RANDOM_IN_REQUIRE = 'random_in_req',
    NONE = 'none',
    RANDOM_OUT_OF_REQUIRE = 'random_out_of_req',
    MIN = 'min',
    MAX = 'max'


class Component(ABC):
    def __init__(self, name: str):
        self.__value_type = ValueType.NONE
        self.__valid_value_type = list()
        self.__default_value_type = ValueType.NONE
        self.__has_requirement = False
        self.__name = name

        self.default_value = None

    @property
    def value_type(self):
        return self.__value_type

    @value_type.setter
    def value_type(self, _type):
        if _type in self.valid_value_type:
            self.__value_type = _type
        else:
            self.__value_type = self.default_value_type

    @property
    def default_value_type(self):
        return self.__default_value_type

    @default_value_type.setter
    def default_value_type(self, _type):
        self.__default_value_type = _type

    @property
    def valid_value_type(self) -> List[ValueType]:
        return self.__valid_value_type

    def add_valid_value_type(self, _type: ValueType):
        self.__valid_value_type.append(_type)

    @property
    def has_requirement(self):
        return self.__has_requirement

    @has_requirement.setter
    def has_requirement(self, _has_requirement):
        self.__has_requirement = _has_requirement

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, _name: str):
        self.__name = _name

    @abstractmethod
    def get(self):
        raise NotImplementedError

    def serialize(self) -> list:
        return [self]


class Countable(ABC):
    @abstractmethod
    def _getMin(self):
        raise NotImplementedError

    @abstractmethod
    def _getMax(self):
        raise NotImplementedError


class RequireRandom(ABC):
    @abstractmethod
    def _getRandomInRequirements(self):
        raise NotImplementedError

    @abstractmethod
    def _getRandomOutOfRequirements(self):
        raise NotImplementedError


class Random(ABC):
    @abstractmethod
    def _getRandom(self):
        raise NotImplementedError


class CountableRandomComponent(Component, Countable, Random, RequireRandom, ABC):
    def __init__(self, name: str):
        super().__init__(name)
        self.value_type = ValueType.RANDOM_IN_REQUIRE
        self.default_value_type = ValueType.RANDOM_IN_REQUIRE

    def get(self):
        if self.default_value is not None:
            return self.default_value

        value_type = self.value_type
        if not self.has_requirement:
            return self._getRandom()
        elif value_type == ValueType.RANDOM_IN_REQUIRE:
            return self._getRandomInRequirements()
        elif value_type == ValueType.RANDOM_OUT_OF_REQUIRE:
            return self._getRandomOutOfRequirements()
        elif value_type == ValueType.MIN:
            return self._getMin()
        elif value_type == ValueType.MAX:
            return self._getMax()
        else:
            raise AttributeError("Unexpected Value Type : {} {}".format(self.name, value_type))

    def _setRequirement(self, component_attr: list, swagger_dict: dict):
        for com_key in component_attr:
            if com_key in swagger_dict.keys():
                self.has_requirement = True

        if self.has_requirement:
            self.default_value_type = ValueType.RANDOM_IN_REQUIRE
            self.add_valid_value_type(ValueType.RANDOM_IN_REQUIRE)
            self.add_valid_value_type(ValueType.RANDOM_OUT_OF_REQUIRE)


class Object(Component):
    def __init__(self, full_name: str, swagger_object: dict):
        super().__init__(full_name)
        self.__properties = dict()
        properties = None
        if Generator.SWAGGER['OBJECT']['PROPERTIES'] in swagger_object.keys():
            properties = swagger_object[Generator.SWAGGER['OBJECT']['PROPERTIES']].items()
        elif Generator.SWAGGER['OBJECT']['ADDITIONAL_PROPERTIES'] in swagger_object.keys():
            properties = swagger_object[Generator.SWAGGER['OBJECT']['ADDITIONAL_PROPERTIES']].items()
        for name, component in properties:
            self.__properties[name] = generateComponent(full_name+'.'+name, component)

    def get(self):
        if self.default_value is not None:
            return self.default_value

        json_object = dict()
        if len(self.__properties) > 0:
            for name, component in self.__properties.items():
                json_object[name] = component.get()
        return json_object

    def serialize(self) -> list:
        serialize_object = list()
        if len(self.__properties) > 0:
            for name, component in self.__properties.items():
                serialized = component.serialize()
                for sub_component in serialized:
                    serialize_object.append(sub_component)
        return serialize_object

    @property
    def properties(self) -> dict:
        return self.__properties


class Array(CountableRandomComponent):
    def __init__(self, full_name: str, swagger_array: dict):
        super().__init__(full_name)
        self._setRequirement([Generator.SWAGGER['ARRAY']['MIN'], Generator.SWAGGER['ARRAY']['MAX']], swagger_array)
        self.__item_component = generateComponent(full_name+'.ITEM', swagger_array[Generator.SWAGGER['ARRAY']['ITEMS']])
        self.__min = swagger_array[Generator.SWAGGER['ARRAY']['MIN']] \
            if Generator.SWAGGER['ARRAY']['MIN'] in swagger_array.keys() else None
        self.__max = swagger_array[Generator.SWAGGER['ARRAY']['MAX']] \
            if Generator.SWAGGER['ARRAY']['MAX'] in swagger_array.keys() else None
        self.__uniqueItems = swagger_array[Generator.SWAGGER['ARRAY']['UNIQUEITEMS']] \
            if Generator.SWAGGER['ARRAY']['UNIQUEITEMS'] in swagger_array.keys() else None

    def serialize(self) -> list:
        serialized_list = list()
        serialized = self.__item_component.serialize()
        serialized_list.append(self)
        for sub_component in serialized:
            serialized_list.append(sub_component)

        return serialized_list

    def _getMin(self):
        json_array = list()
        for _ in range(self.__min):
            json_array.append(self.__item_component.get())
        return json_array

    def _getMax(self):
        json_array = list()
        for _ in range(self.__max):
            json_array.append(self.__item_component.get())
        return json_array

    def _getRandomInRequirements(self):
        json_array = list()
        for _ in range(randrange(self.__min, self.__max)):
            json_array.append(self.__item_component.get())
        return json_array

    def _getRandomOutOfRequirements(self):
        json_array = list()
        for _ in range(randrange(self.__max+1, self.__max+5)):
            json_array.append(self.__item_component.get())
        return json_array

    def _getRandom(self):
        json_array = list()
        for _ in range(randrange(1, 4)):
            json_array.append(self.__item_component.get())
        return json_array

    @property
    def item_component(self):
        return self.__item_component


class String(CountableRandomComponent):
    rand_regex = r'[^\x00-\x1f|^\s|^\'|^\"]{20}'

    def __init__(self, name: str, swagger_string: dict):
        super().__init__(name)
        req = [Generator.SWAGGER['STRING']['PATTERN'],
               Generator.SWAGGER['STRING']['EXAMPLE'],
               Generator.SWAGGER['STRING']['ENUM'],
               Generator.SWAGGER['STRING']['FORMAT']]
        self.__regex_generator = Xeger()
        self.__min = swagger_string[Generator.SWAGGER['STRING']['MIN']] \
            if Generator.SWAGGER['STRING']['MIN'] in swagger_string.keys() else None
        self.__max = swagger_string[Generator.SWAGGER['STRING']['MAX']] \
            if Generator.SWAGGER['STRING']['MAX'] in swagger_string.keys() else None
        self.__pattern = swagger_string[Generator.SWAGGER['STRING']['PATTERN']] \
            if Generator.SWAGGER['STRING']['PATTERN'] in swagger_string.keys() else None
        self.__example = swagger_string[Generator.SWAGGER['STRING']['EXAMPLE']] \
            if Generator.SWAGGER['STRING']['EXAMPLE'] in swagger_string.keys() else None
        self.__enum = swagger_string[Generator.SWAGGER['STRING']['ENUM']] \
            if Generator.SWAGGER['STRING']['ENUM'] in swagger_string.keys() else None
        self.__format = swagger_string[Generator.SWAGGER['STRING']['FORMAT']] \
            if Generator.SWAGGER['STRING']['FORMAT'] in swagger_string.keys() else None
        self._setRequirement(req, swagger_string)

        if self.__min is not None:
            self.add_valid_value_type(ValueType.MIN)
        if self.__max is not None:
            self.add_valid_value_type(ValueType.MAX)

    def __getExample(self):
        json_string = self.__example
        return json_string

    def __getEnum(self):
        json_string = self.__enum[randrange(0, len(self.__enum))]
        return json_string

    def __getFormat(self):
        if Generator.SWAGGER['FORMAT']['STRING']['DATETIME'] == self.__format:
            return "{:%Y-%m-%dT%H:%M:%S.}".format(datetime.now())+"{:%f}".format(datetime.now())[:3]+'Z'
        else:
            raise NotImplementedError

    def __getPattern(self):
        return self.__regex_generator.xeger(self.__pattern)

    def _getMin(self):
        json_string = self.__regex_generator.xeger('[a-zA-Z]{'+str(self.__min)+'}')
        return json_string

    def _getMax(self):
        json_string = self.__regex_generator.xeger('[a-zA-Z]{'+str(self.__max)+'}')
        return json_string

    def _getRandomInRequirements(self):
        if self.__enum is not None:
            return self.__getEnum()
        elif self.__format is not None:
            return self.__getFormat()
        elif self.__pattern is not None:
            return self.__getPattern()
        elif self.__example is not None:
            return self.__getExample()
        else:
            raise AttributeError

    def _getRandomOutOfRequirements(self):
        return self._getRandom()

    def _getRandom(self):
        return self.__regex_generator.xeger('[a-zA-Z]{8}')


class Integer(CountableRandomComponent):
    __int32_min = -2 ** 31
    __int32_max = 2 ** 31 - 1
    __int64_min = -2 ** 63
    __int64_max = 2 ** 63 - 1

    def __init__(self, name: str, swagger_integer: dict):
        super().__init__(name)
        self._setRequirement(list(Generator.SWAGGER['NUMERIC'].values()), swagger_integer)

        self.__min = swagger_integer[Generator.SWAGGER['NUMERIC']['MIN']] \
            if Generator.SWAGGER['NUMERIC']['MIN'] in swagger_integer.keys() else None
        self.__max = swagger_integer[Generator.SWAGGER['NUMERIC']['MAX']] \
            if Generator.SWAGGER['NUMERIC']['MAX'] in swagger_integer.keys() else None

        if Generator.SWAGGER['NUMERIC']['FORMAT'] in swagger_integer.keys():
            self.__format = swagger_integer[Generator.SWAGGER['NUMERIC']['FORMAT']]
            if self.__min is None:
                if self.__format == Generator.SWAGGER['FORMAT']['INTEGER']['INT32']:
                    self.__min = Integer.__int32_min
                elif self.__format == Generator.SWAGGER['FORMAT']['INTEGER']['INT64']:
                    self.__min = Integer.__int64_min
            if self.__max is None:
                if self.__format == Generator.SWAGGER['FORMAT']['INTEGER']['INT32']:
                    self.__max = Integer.__int32_max
                elif self.__format == Generator.SWAGGER['FORMAT']['INTEGER']['INT64']:
                    self.__max = Integer.__int64_max

        self.add_valid_value_type(ValueType.MIN)
        self.add_valid_value_type(ValueType.MAX)

    def _getMin(self):
        return self.__min

    def _getMax(self):
        return self.__max

    def _getRandomInRequirements(self):
        json_integer = randrange(self.__min, self.__max, 1)
        return json_integer

    def _getRandomOutOfRequirements(self):
        under_require = randrange(self.__min - 10000, self.__min - 1, 1)
        over_require = randrange(self.__max + 1, self.__max + 10000, 1)
        return under_require if random.random() > 0.5 else over_require

    def _getRandom(self):
        return randrange(Integer.__int64_min, Integer.__int64_max, 1)


class Number(CountableRandomComponent):
    __float_min = 1.175494351 * 10 ** -38
    __float_max = 1.175494351 * 10 ** -38
    __double_min = 2.2250738585072014 * 10 ** -308
    __double_max = 1.7976931348623158 * 10 ** -308

    def __init__(self, name: str, swagger_number: dict):
        super().__init__(name)
        self._setRequirement(list(Generator.SWAGGER['NUMERIC'].values()), swagger_number)

        self.__min = swagger_number[Generator.SWAGGER['NUMERIC']['MIN']] \
            if Generator.SWAGGER['NUMERIC']['MIN'] in swagger_number.keys() else None
        self.__max = swagger_number[Generator.SWAGGER['NUMERIC']['MAX']] \
            if Generator.SWAGGER['NUMERIC']['MAX'] in swagger_number.keys() else None

        if Generator.SWAGGER['NUMERIC']['FORMAT'] in swagger_number.keys():
            self.__format = swagger_number[Generator.SWAGGER['NUMERIC']['FORMAT']]
            if self.__min is None:
                if self.__format == Generator.SWAGGER['FORMAT']['NUMBER']['FLOAT']:
                    self.__min = Number.__float_min
                elif self.__format == Generator.SWAGGER['FORMAT']['NUMBER']['FLOAT']:
                    self.__min = Number.__double_min
            if self.__max is None:
                if self.__format == Generator.SWAGGER['FORMAT']['NUMBER']['DOUBLE']:
                    self.__max = Number.__float_max
                elif self.__format == Generator.SWAGGER['FORMAT']['NUMBER']['DOUBLE']:
                    self.__max = Number.__double_max

        self.add_valid_value_type(ValueType.MIN)
        self.add_valid_value_type(ValueType.MAX)

    def _getMin(self):
        return self.__min

    def _getMax(self):
        return self.__max

    def _getRandomInRequirements(self):
        # can apply attribute
        json_number = random.uniform(self.__min, self.__max)
        return json_number

    def _getRandomOutOfRequirements(self):
        under_require = random.uniform(self.__min-10000, self.__min-1)
        over_require = random.uniform(self.__max+1, self.__max+10000)
        return under_require if random.random() > 0.5 else over_require

    def _getRandom(self):
        return random.uniform(Number.__double_min, Number.__double_max)


class Boolean(Component, Countable):
    def __init__(self, name: str):
        super().__init__(name)
        self.default_value_type = self.value_type = ValueType.MAX
        self.add_valid_value_type(ValueType.MIN)
        self.add_valid_value_type(ValueType.MAX)

    def get(self):
        if self.default_value is not None:
            return self.default_value
        value_type = super().value_type
        if value_type == ValueType.MIN:
            return self._getMin()
        elif value_type == ValueType.MAX:
            return self._getMax()
        else:
            raise AttributeError("Unexpected Value Type : {}".format(self.value_type))

    def _getMin(self):
        return False

    def _getMax(self):
        return True


class File(Component, RequireRandom):
    def __init__(self, name: str):
        super().__init__(name)
        self.value_type = ValueType.RANDOM_IN_REQUIRE
        self.has_requirement = True
        self.add_valid_value_type(ValueType.RANDOM_IN_REQUIRE)
        self.add_valid_value_type(ValueType.RANDOM_OUT_OF_REQUIRE)

        self.__valid_files = list()
        self.__invalid_files = list()

        invalid_directory = 'files/invalid.txt'
        self.__invalid_files.append(invalid_directory)

        valid_directory = 'files/invalid.txt'
        self.__valid_files.append(valid_directory)

    def get(self):
        if self.default_value is not None:
            return self.default_value

        value_type = self.value_type
        if value_type == ValueType.RANDOM_IN_REQUIRE:
            return self._getRandomInRequirements()
        elif value_type == ValueType.RANDOM_OUT_OF_REQUIRE:
            return self._getRandomOutOfRequirements()
        else:
            raise AttributeError("Unexpected Value Type : {}".format(self.value_type))

    def _getRandomInRequirements(self):
        file_index = random.randrange(0, len(self.__valid_files))
        binary_file = openFileFromCurrentDir(self.__valid_files[file_index], 'rb')
        return binary_file

    def _getRandomOutOfRequirements(self):
        file_index = random.randrange(0, len(self.__invalid_files))
        binary_file = openFileFromCurrentDir(self.__invalid_files[file_index], 'rb')
        return binary_file


class Components:
    def __init__(self):
        self.__components = dict()

    def add(self, swagger_name: str, swagger_components: Component):
        self.__components[swagger_name] = swagger_components

    def get(self):
        json_dict = dict()
        for name, component in self.__components.items():
            json_dict[name] = component.get()
        return json_dict

    def serialize(self) -> List[Component]:
        serialized_list = list()
        for name, component in self.__components.items():
            serialized = component.serialize()
            for sub_component in serialized:
                serialized_list.append(sub_component)
        return serialized_list


class Query(Components):
    def get(self):
        queries = Components.get(self)
        return queriesToQueryString(queries)


class Body(Components):
    def get(self):
        components = Components.get(self)
        if 'body' in components.keys():
            return components['body']
        else:
            return components


def generateComponent(name: str, schema: dict) -> Component:
    if Generator.SWAGGER['SINGLE_VALUE']['TYPE'] in schema.keys():
        if Generator.SWAGGER['TYPE']['OBJECT'] == schema[Generator.SWAGGER['SINGLE_VALUE']['TYPE']]:
            return Object(name, schema)
        elif Generator.SWAGGER['TYPE']['ARRAY'] == schema[Generator.SWAGGER['SINGLE_VALUE']['TYPE']]:
            return Array(name, schema)
        elif Generator.SWAGGER['TYPE']['NUMBER'] == schema[Generator.SWAGGER['SINGLE_VALUE']['TYPE']]:
            return Number(name, schema)
        elif Generator.SWAGGER['TYPE']['INTEGER'] == schema[Generator.SWAGGER['SINGLE_VALUE']['TYPE']]:
            return Integer(name, schema)
        elif Generator.SWAGGER['TYPE']['STRING'] == schema[Generator.SWAGGER['SINGLE_VALUE']['TYPE']]:
            return String(name, schema)
        elif Generator.SWAGGER['TYPE']['BOOL'] == schema[Generator.SWAGGER['SINGLE_VALUE']['TYPE']]:
            return Boolean(name)
        elif Generator.SWAGGER['TYPE']['FILE'] == schema[Generator.SWAGGER['SINGLE_VALUE']['TYPE']]:
            return File(name)
    if Generator.SWAGGER['SINGLE_VALUE']['REF'] in schema.keys():
        return findReference(name, schema)
    raise NotImplementedError()


def findReference(name: str, swagger_ref: dict) -> Component:
    definitions = swagger_ref[Generator.SWAGGER['SINGLE_VALUE']['REF']].split('/')[1:]
    definition = generator.swagger
    sub_def = None
    for sub_def in definitions:
        definition = definition[sub_def]
    if '$ref' in swagger_ref.keys():
        return parseParameter(name+':'+sub_def, definition)
    elif 'type' in definition.keys():
        return generateComponent(name+'.'+sub_def, definition)
    else:
        raise AttributeError


def generateRequestArgument(parameters: list) -> Dict[str, Components]:
    if generator.swagger is None:
        raise Exception('You must load swagger file. should import loadSwaggerJSON and add Swagger file to Library')
    swagger_description = {
        'path': Components(),
        'body': Body(),
        'formData': Components(),
        'query': Query(),
        'header': Components()
    }
    for parameter in parameters:
        if 'in' in parameter.keys():
            param_in = parameter['in']
            param_name = parameter['name']

            if not param_name == 'body':
                path = param_in + '.' + param_name
            else:
                path = param_in
            if param_in in Generator.SWAGGER['IN'].values():
                swagger_description[param_in].add(param_name, parseParameter(path, parameter))
            else:
                raise AttributeError()
        else:
            param_name = 'body'
            swagger_description[Generator.SWAGGER['IN']['BODY']].add(param_name, parseParameter(param_name, parameter))

    return swagger_description


def parseParameter(name: str, parameter: dict) -> Component:
    if 'in' in parameter.keys():  # is parameter
        param_in = parameter['in']

        if param_in == Generator.SWAGGER['IN']['BODY']:
            return generateComponent(name, parameter['schema'])
        elif param_in in Generator.SWAGGER['IN'].values():
            return generateComponent(name, parameter)
        else:
            raise AttributeError()
    else:  # not param. component
        return generateComponent(name, parameter)


def queriesToQueryString(json_queries: dict) -> str:
    query = ''
    for key, val in json_queries.items():
        if isinstance(val, list):
            for element in val:
                query = query + '{}={}&'.format(key, element)
        else:
            query = query + '{}={}&'.format(key, val)
    return query[:-1]
