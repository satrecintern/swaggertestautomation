"""
Swagger 해석기
Swagger 최상단에 존재하는 정보를 읽어서 반환해주는 함수 제공

"""


import re


def getRequestPaths(swagger: dict):
    return swagger['paths']


def getBasePath(swagger: dict):
    if 'schemes' not in swagger.keys():
        return 'http://' + swagger['host'] + swagger['basePath']
        # raise AttributeError("schemes missing")
    else:
        return swagger['schemes'][-1] + '://' + swagger['host'] + swagger['basePath']


def fillPath(path: str, params: dict) -> str:
    arg = re.compile('{.*?}').findall(path)
    args = [re.sub('[{}]', '', x) for x in arg]

    if len(args) == 0:
        return path
    else:
        for param in args:
            if param in params.keys():
                path = path.replace('{'+param+'}', str(params[param]), 1)
    return path


def getFirstRequestPath(paths: dict) -> tuple:
    return next(iter(paths.items()))


def getFirstRequest(path: dict) -> tuple:
    return next(iter(path.items()))


def getParameters(request: dict) -> list:
    if 'parameters' in request.keys():
        return request['parameters']
    else:
        return []


def getResponses(request: dict) -> dict:
    if 'responses' in request.keys():
        return request['responses']
    else:
        return {}


def getProduces(request: dict) -> list:
    if 'produces' in request.keys():
        return request['produces']
    else:
        return []
