from typing import List

from RESTester.reader import *
from RESTester.component import generateRequestArgument, ValueType, Components, Component, \
    Countable, RequireRandom, loadSwaggerJSON
from RESTester.request import request
from RESTester.output import *
from RESTester.validator import validateResult, validateResultWithTestCase


class TestResult:
    def __init__(self):
        self.count = 0
        self.success = 0
        self.fail = 0
        self.skip = 0
        self.data = dict()

    def countTestResult(self, response_result: str):
        if response_result == 'success':
            self.success += 1
        elif response_result == 'pass':
            self.skip += 1
        else:
            self.fail += 1
        self.count += 1


def testSimple(swagger: dict):
    test = TestResult()
    loadSwaggerJSON(swagger)
    basePath = getBasePath(swagger)
    paths = getRequestPaths(swagger)
    ALL = 'ALL.COMPONENT'

    for requestPath, pathInfo in paths.items():
        test.data[requestPath] = dict()
        for method, requestInfo in pathInfo.items():
            print('{}: {}'.format(requestPath, method))
            test.data[requestPath][method] = dict()
            params_prototype = getParameters(requestInfo)
            responses = getResponses(requestInfo)
            produces = getProduces(requestInfo)
            args = generateRequestArgument(params_prototype)
            components = list()
            for _components in args.values():
                for _component in _components.serialize():
                    components.append(_component)
                    print(_component.name)
            anyRequirement = anyComponentHasRequirement(components)
            if anyRequirement:
                value_list = list(ValueType)
                value_list.remove(ValueType.NONE)
            else:
                value_list = [ValueType.NONE]

            test.data[requestPath][method][ALL] = dict()
            for value_type in value_list:
                print('\t\t\t' + value_type.name)
                setAllComponentType(components, value_type)
                result = testComponent(args, basePath, requestPath, method, produces)
                result['state'] = validateResult(result['response'], value_type, anyRequirement, responses)
                test.countTestResult(result['state'])
                test.data[requestPath][method][ALL][value_type.name] = result

            setAllComponentType(components, ValueType.RANDOM_IN_REQUIRE)

    reportRequestAndResponse(basePath, test.data, {'count': test.count, 'success': test.success, 'fail': test.fail,
                                                   'skip': test.skip})


def testSimpleWithGeneratedData(swagger: dict, test: dict, args: dict):
    print('#######################################')
    print('#              Test                   #')
    print('#######################################')

    test_result = TestResult()
    loadSwaggerJSON(swagger)
    basePath = getBasePath(swagger)

    for path, path_info in test.items():
        test_result.data[path] = dict()
        for method, request_infos in path_info.items():
            test_result.data[path][method] = list()
            for request_info in request_infos:

                components = list()
                for param_type, _components in args[path][method].items():
                    for _component in _components.serialize():
                        if _component.name in request_info['request_params'].keys():
                            _component.default_value = request_info['request_params'][_component.name]
                produces = getProduces(request_info)
                setAllComponentType(components, ValueType.RANDOM_IN_REQUIRE)
                result = testComponent(args[path][method], basePath, path, method, produces)
                result['state'] = validateResultWithTestCase(result['response'], request_info['response_code'])
                result['expected_response_code'] = request_info['response_code']
                test_result.countTestResult(result['state'])
                print('{}: {}'.format(path, method))
                print('expected {}. {}'.format(request_info['response_code'], result['state']))
                test_result.data[path][method].append(result)

    reportRequestAndResponseWithTestCase(basePath, test_result.data,
                                         {
                                             'count': test_result.count,
                                             'success': test_result.success,
                                             'fail': test_result.fail,
                                             'skip': test_result.skip
                                         })


def setAllComponentType(components: List[Component], value_type: ValueType):
    for component in components:
        setComponentType(component, value_type)


def setComponentType(component: Component, value_type: ValueType):
    if not isinstance(component, RequireRandom) and \
            (value_type == ValueType.RANDOM_IN_REQUIRE or value_type == ValueType.RANDOM_OUT_OF_REQUIRE):
        return
    elif not isinstance(component, Countable) and \
            (value_type == ValueType.MIN or value_type == ValueType.MAX):
        return
    component.value_type = value_type


def testComponent(args: Dict[str, Components], basePath: str, requestPath: str, method: str, produces: List[str]):
    params = args['path'].get()
    data = args['body'].get()
    files = args['formData'].get()
    query = args['query'].get()
    header = args['header'].get()
    if query == '':
        totalPath = basePath + requestPath
        query = None
    else:
        totalPath = basePath + requestPath + "?" + query

    totalPath = fillPath(totalPath, params)

    delay = 1
    while True:
        response = request(method, totalPath, data, params, files, header, produces)
        if response.status_code == 429:
            time.sleep(delay)
            delay += 1
        else:
            break

    res = {
        'request': {
            'path': toString(params),
            'data': toString(data),
            'formData': toString(files),
            'query': query,
            'header': toString(header)
        },
        'response': response,
        'state': ''
    }
    return res


def anyComponentHasRequirement(components: List[Component]):
    for component in components:
        if component.has_requirement:
            return True
    return False
