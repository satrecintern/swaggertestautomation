import json
import re
import time
from typing import Dict
from jinja2 import Template


TAB = '  '


def reportRequestAndResponse(basePath: str, context: dict, test_count: Dict[str, int]):
    with open('template.html', 'r') as f:
        template = Template(f.read())
    with open('result/test_result_{}.html'.format(time.strftime('%Y%m%d_%H%M%S')), 'w') as f:
        f.write(template.render(basePath=basePath, result=context, test_count=test_count))


def reportRequestAndResponseWithTestCase(basePath: str, context: dict, test_count: Dict[str, int]):
    with open('template_custom.html', 'r') as f:
        template = Template(f.read())
    with open('result/test_result_{}.html'.format(time.strftime('%Y%m%d_%H%M%S')), 'w') as f:
        f.write(template.render(basePath=basePath, result=context, test_count=test_count))


def saveTest(test: dict):
    with open('test/test_{}.json'.format(time.strftime('%Y%m%d_%H%M%S')), 'w') as f:
        f.write(json.dumps(test, indent=4))


def saveTestCase(method, parameters, data, files, query, response):
    print(TAB + method)

    print(TAB * 2 + "REQUEST:")
    if len(parameters) > 0:
        print(TAB * 3 + "PARAM: ")
        print(TAB * 3 + toStringDict(parameters))
    if len(query) > 0:
        print(TAB * 3 + "QUERY: " + query)
    if len(data) > 0:
        print(TAB * 3 + "DATA:")
        if isinstance(data, list):
            print(TAB * 3 + toStringList(data))
        else:
            print(TAB * 3 + toStringDict(data))
    if len(files) > 0:
        print(TAB * 3 + "FILE: ")
        print(TAB * 3 + toStringDict(files))

    print('\r\n')
    print(TAB * 2 + "RESPONSE: " + str(response.status_code))

    elapsed_ms = str(int(float("0." + str(response.elapsed).split('.')[1]) * 1000))
    print(TAB * 4 + "ELAPSED: " + elapsed_ms + "ms")
    print(TAB * 4 + "HEADER: " + str(response.headers))
    print(TAB * 4 + "BODY: ")
    print(response.text.replace('\n', '\r\n'))
    print('--------------------------------------\r\n')


def printRequestAndResponse(method, parameters, data, files, query, response):
    print(TAB + method)
    print(TAB*2 + "REQUEST:")
    if len(parameters) > 0:
        print(TAB*3 + "PARAM: ")
        print(TAB*3 + toStringDict(parameters))
    if len(query) > 0:
        print(TAB*3 + "QUERY: " + query)
    if len(data) > 0:
        print(TAB*3 + "DATA:")
        if isinstance(data, list):
            print(TAB*3 + toStringList(data))
        else:
            print(TAB*3 + toStringDict(data))
    if len(files) > 0:
        print(TAB*3 + "FILE: ")
        print(TAB*3 + toStringDict(files))

    print('\r\n')
    print(TAB*2 + "RESPONSE: " + str(response.status_code))

    elapsed_ms = str(int(float("0."+str(response.elapsed).split('.')[1])*1000))
    print(TAB*4 + "ELAPSED: " + elapsed_ms + "ms")
    # print("\t\t\t\tHEADER: " + str(response.headers))
    print(TAB*4 + "BODY: ")
    print(response.text.replace('\n', '\r\n'))
    print('--------------------------------------\r\n')


def toString(json_structure):
    if isinstance(json_structure, dict):
        res = toStringDict(json_structure, 1)
    elif isinstance(json_structure, list):
        res = toStringList(json_structure, 1)
    else:
        return None

    if re.match('[{|\[](\\r\\n)+[}|\]]', res):
        return None
    else:
        return res.replace("<", "&lt;").replace(">", "&gt;")


def toStringDict(json_object: dict, depth: int = 4) -> str:
    k_v = ''
    for (key, val) in json_object.items():
        if isinstance(val, str):
            val_str = "'{}'".format(val)
        elif isinstance(val, dict):
            val_str = toStringDict(val, depth+1)
        elif isinstance(val, list):
            val_str = toStringList(val, depth+1)
        else:
            val_str = str(val)
        k_v = k_v + ' '*(2*depth) + ': '.join((key, val_str)) + ',\r\n'
    res = '{\r\n' + k_v[:-3] + '\r\n' + ' '*(2*(depth-1)) + '}'
    return res


def toStringList(json_list: list, depth: int = 4) -> str:
    k_v = ''
    for val in json_list:
        if isinstance(val, str):
            val_str = "'{}'".format(val)
        elif isinstance(val, dict):
            val_str = toStringDict(val, depth+1)
        elif isinstance(val, list):
            val_str = toStringList(val, depth+1)
        else:
            val_str = str(val)
        k_v = k_v + ' '*(2*depth) + val_str + ',\r\n'

    res = '[\r\n'+k_v[:-3]+'\r\n' + ' '*(2*(depth-1)) + ']'
    return res


def printPaths(paths: dict) -> None:
    for path in paths:
        print(path)
        for req_type, req_body in paths[path].items():
            print("    " + req_type)
            if len(req_body["parameters"]) > 0:
                print("\t\tPARAM :")
            for parameter in req_body["parameters"]:
                if 'type' not in parameter.keys():
                    param_type = 'object'
                else:
                    param_type = parameter['type']
                print("\t\t\t{} : {}".format(parameter['name'], param_type))

            print("\t\tResponse :")
            for status, res_body in req_body["responses"].items():
                print("\t\t\t[{}]".format(status))
                if 'schema' in res_body.keys():
                    print("\t\t\t\tMSG : {}\n\t\t\t\tBODY : {}".format(status, res_body['description'],
                                                                       res_body['schema']))
                else:
                    print("\t\t\t\tMSG : {}".format(status, res_body['description']))
