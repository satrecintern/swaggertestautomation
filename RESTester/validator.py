from requests import Response

from RESTester.component import ValueType


def isRequire(value_type: ValueType):
    return (value_type == ValueType.MIN) or \
           (value_type == ValueType.MAX) or \
           (value_type == ValueType.RANDOM_IN_REQUIRE)


def validateResultWithTestCase(response: Response, expected_response: int) -> str:
    if response.status_code == expected_response:
        return 'success'
    else:
        return 'fail'


def validateResult(response: Response, value_type: ValueType, anyRequirement: bool, expected_responses: dict) -> str:
    if response.status_code >= 500:
        return 'fail'

    if response.status_code == 400:
        return 'pass'

    if response.status_code == 405:
        for status_code in expected_responses.keys():
            if 405 == status_code:
                return 'success'
        return 'fail'

    if anyRequirement:  # 실제 컴포넌트에 요구사항이 존재
        if isRequire(value_type):  # 요구사항을 만족하는 요청
            if response.status_code < 300:  # 정상 응답
                return 'success'
            else:  # 비정상 응답
                return 'fail'
        else:  # 요구사항을 만족하지 않는 요청
            if response.status_code < 300:  # 정상 응답
                return 'fail'
            else:  # 비정상 응답
                return 'success'
    else:  # 실제 요구사항이 없음
        if response.status_code < 300:  # 정상 응답
            return 'success'
        else:  # 비정상 응답
            return 'fail'
