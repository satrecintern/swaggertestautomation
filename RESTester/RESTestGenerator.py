import json

from RESTester.component import loadSwaggerJSON, generateRequestArgument, ValueType, Array, Integer, Number, \
    Component, Object, Components
from RESTester.output import saveTest, toString
from RESTester.reader import *
from RESTester.test import setAllComponentType, Dict


def loadTestCase(testfile: str) -> dict:
    return json.load(open(testfile, 'r'))


def getStructure(swagger: dict) -> dict:
    loadSwaggerJSON(swagger)
    paths = getRequestPaths(swagger)

    structure = dict()
    for requestPath, pathInfo in paths.items():
        structure[requestPath] = dict()
        for method, requestInfo in pathInfo.items():
            params_prototype = getParameters(requestInfo)
            structure[requestPath][method] = generateRequestArgument(params_prototype)
    return structure


def generateSimpleTest(swagger: dict) -> dict:
    print('#######################################')
    print('#     Generate Test Case              #')
    print('#######################################')
    loadSwaggerJSON(swagger)
    paths = getRequestPaths(swagger)
    base_path = getBasePath(swagger)
    test = dict()
    print(base_path)

    def generate():
        input_next_before = False
        for requestPath, pathInfo in paths.items():
            test[requestPath] = dict()
            for method, requestInfo in pathInfo.items():
                print('{}: {}'.format(requestPath, method))
                test[requestPath][method] = list()
                params_prototype = getParameters(requestInfo)
                args = generateRequestArgument(params_prototype)
                components = list()

                expected_responses = ', '.join([str(code) for code, val in requestInfo['responses'].items()])

                for _components in args.values():
                    for _component in _components.serialize():
                        components.append(_component)
                setAllComponentType(components, ValueType.RANDOM_IN_REQUIRE)

                showArguments(args)

                while True:
                    response_code = input('response code [expected {}]\n'.format(expected_responses))

                    if response_code == 'next':
                        input_next_before = True
                        if len(test[requestPath][method]) == 0:
                            del test[requestPath][method]
                        break
                    elif response_code == 'end':
                        if len(test[requestPath][method]) == 0:
                            del test[requestPath][method]
                        return
                    elif response_code == '' and input_next_before:
                        print('next\n')
                        if len(test[requestPath][method]) == 0:
                            del test[requestPath][method]
                        break

                    try:
                        response_code = int(response_code)
                        input_next_before = False
                    except ValueError:
                        continue
                    if 100 > response_code or response_code > 500:
                        print('invalid response code')
                        continue

                    test_data = dict()
                    test_data['response_code'] = response_code
                    test_data['request_params'] = dict()
                    test[requestPath][method].append(test_data)
                    array_name = ''
                    for component in components:
                        if array_name != '' and component.name.__contains__(array_name):
                            continue
                        if isinstance(component, Array):
                            array_name = component.name
                        res = inputComponent(component)
                        test_data['request_params'][component.name] = res
                    print()

    generate()
    saveTest(test)
    return test


def showArguments(args: Dict[str, Components]):
    for key, components in args.items():
        stringify = toString(components.get())
        if stringify is not None:
            print('<< example : {} >>'.format(key))
            print(stringify+'\n')


def inputComponent(component: Component):
    if isinstance(component, Object):
        result = inputObject(component)
    elif isinstance(component, Array):
        result = inputArray(component)
    else:
        result = inputLegacy(component)
    return result


def inputObject(component: Object) -> dict:
    result = dict()
    for name, sub_component in component.properties.items():
        result[name] = inputComponent(sub_component)
    return result


def inputArray(component: Array) -> list:
    default_value = component.get()
    size = input('{}\t: {} [default = {}]\ntype size = '
                 .format(type(component).__name__, component.name, default_value))
    if size == '':
        result = default_value
    else:
        result = list()
        size = int(size)
        for i in range(size):
            print('{} of {}'.format(i+1, size))
            result.append(inputComponent(component.item_component))
    return result


def inputLegacy(component: Component) -> str:
    default_value = component.get()
    result = input('{}\t: {} [default = {}]\n'
                   .format(type(component).__name__, component.name, default_value))
    if result == '':
        result = default_value
        print(result)
    elif isinstance(component, Integer):
        result = int(result)
    elif isinstance(component, Number):
        result = float(result)
    return result
