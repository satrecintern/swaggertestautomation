import json
from typing import List

import requests.sessions
from requests import Response
# from requests.auth import HTTPBasicAuth # for auth

session = requests.session()


def request(method: str, path: str, data: str = None, params: dict = None, files: dict = None, header: dict = None,
            content_list: List[str] = None) -> Response:
    if len(files) == 0:
        content_type = 'application/json'
    elif 'file' in files.keys():
        content_type = 'multipart/form-data; boundary=----WebKitFormBoundaryp7MA4YWxkTrZu0gW'
    else:
        content_type = 'application/x-www-form-urlencoded'

    if len(content_list) > 0:
        accept_type = content_list[0]
        if 'application/json' in content_list:
            accept_type = 'application/json'
    else:
        accept_type = 'application/json'

    if header is None:
        header = dict()
    header['Accept'] = accept_type
    header['Content-Type'] = content_type
    args = {
        'url': path,
        'headers': header,
        # 'timeout': 1000000, # for debug
        # 'auth': HTTPBasicAuth('admin', 'geoserver') # need for 401, 403
    }
    if len(data) > 0:
        args['data'] = json.dumps(data)
    if len(params) > 0:
        args['params'] = params
    if len(files) > 0:
        args['files'] = files

    if method == 'get':
        return session.get(**args)
    elif method == 'post':
        return session.post(**args)
    elif method == 'put':
        return session.put(**args)
    elif method == 'delete':
        return session.delete(**args)
    else:
        raise AttributeError("Unexpected request method : {}".format(method))
